#include <stdio.h>
#include <time.h>

#include "file.h"
#include "select.h"
#include "struct.h"


/**
 * Time the select algorithm and print the cpu time to a file.
 *
 * param arr        Array to select from (passed to select_i)
 * @param n         Size of subgroups (passed to select_i)
 * @param i         i-th smallest element to select (passed to select_i)
 * @param filename  Filename of output file
 * @param fn        Function to time (should use select_i)
 */
void time_fn(Array* arr, int n, int i, char* filename, int (*fn)(Array*, int, int, int, int))
{
    clock_t start, end;
    double time;
    FILE* file = 0;
    int res;

    // Time function
    start = clock();
    res = fn(arr, 0, arr->size, n, i);
    end = clock();

    printf("%d", res);
    time = ((double) end - start) / CLOCKS_PER_SEC;

    // Output to file
    file = fopen(filename, "a");
    fprintf(file, "%zu, %d, %f\n", arr->size, n, time);
    fclose(file);

    printf("Results written to %s\n", filename);
}

int main(int argc, char* argv[])
{
    // Read file
    Array* array = read(argv[1]);

    // Time and print to output
    time_fn(array, atoi(argv[2]), atoi(argv[3]), argv[4], &select_i);

    free(array->arr);
    free(array);

    return 0;
}