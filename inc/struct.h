#ifndef HW2_ARRAY_H
#define HW2_ARRAY_H


// Basic integer array implementation. Store the length of an array along with the size.
typedef struct {
    int * arr;
    size_t size;
} Array;


#endif //HW2_ARRAY_H
