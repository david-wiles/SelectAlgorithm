#include <stdio.h>
#include <stdlib.h>

#include "file.h"

// Parse input into int array
Array* parse_input(const char* input, long size)
{
    int* num_array;
    Array* array = malloc(sizeof(Array));
    int n = 1;
    int cindex = 0, nindex = 0;
    char* next_num = malloc(10 * sizeof(char));

    // Count number of spaces to determine number of elements
    for (long i = 0; i < size; ++i) {
        if (input[i] == ' ')
            n++;
    }

    num_array = malloc(n * sizeof(int));

    // If a space is read, parse the temp string into an integer. Otherwise, concatenate char to temp
    for (long i = 0; i < size + 1; ++i) {
        if (!input[i] || input[i] == ' ') {
            n++;
            num_array[nindex++] = atoi(next_num);
            cindex = 0;
            free(next_num);
            next_num = malloc(10 * sizeof(char));
        } else {
            next_num[cindex++] = input[i];
        }
    }

    free(next_num);

    array->arr = num_array;
    array->size = (size_t) nindex;
    return array;
}

// Read an input file
Array* read(char* filename)
{
    FILE* file = 0;
    long len = 0;
    char* buffer = 0;
    Array *array = NULL;
    file = fopen(filename, "r");

    if (file) {
        // Read file into char buffer
        fseek(file, 0, SEEK_END);
        len = ftell(file);
        fseek(file, 0, SEEK_SET);
        buffer = malloc(len);
        if (buffer) {
            fread(buffer, 1, len, file);
        }
        fclose(file);

        // Parse into int array
        array = parse_input(buffer, len);

        free(buffer);
    }

    return array;
}
