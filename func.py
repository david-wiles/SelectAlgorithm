import os
import random


# Make an array of random unique ints of  specified size
def make_array(size):
    arr = random.sample(range(0, size), size)
    filename = "input/" + str(size) + ".in"
    f = open(filename, "w")
    for i in arr:
        f.write(str(i) + " ")

    f.close()


# Make all arrays in predetermined range
def make_all_arrays():
    for n in (i * 10 ** exp for exp in range(4, 6) for i in range(1, 10)):
        make_array(n)


def run_test(size, n, i):
    input_file = "input/" + str(size) + ".in"
    output_file = "output/" + str(size) + ".out"
    os.system(f"./Select {input_file} {str(n)} {str(i)} {output_file}")

# Run all tests with predetermined input size
def run_tests(i):
    # Run test for each input file
    for f in (x * 10 ** exp for exp in range(4, 6) for x in range(1, 10)):
        # Execute test with 100 different select values and all n values
        for n in [3, 5, 7, 9]:
            run_test(f, n, i)


if __name__ == '__main__':
    run_test(10000, 5, 1000)
