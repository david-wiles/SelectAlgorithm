#include "select.h"

// math.h ceil
int ceiling(double x)
{
    int num = (int) x;
    if (x == (double) num)
        return num;
    else
        return num + 1;
}

/**
 * Swap two elements of an int array. No bounds checking is performed, errors could occur if invalid
 *
 * @param arr   Array to use
 * @param i     First location
 * @param j     Second location
 */
 void exchange(int* arr, int i, int j)
{
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

/**
 * Partition a portion of an array and return the index of pivot after the operation
 *
 * @param arr   Array to partition
 * @param lower Lower index of the partition
 * @param upper Upper index of the partition
 * @param index The index of the pivot value
 * @return      Final index of pivot
 */
 int partition(int* arr, int lower, int upper, int index)
{
    exchange(arr, index, upper - 1);

    int pivot = arr[upper - 1];
    int i = lower - 1;

    for (int j = lower; j < upper - 2; ++j) {
        if (arr[j] <= pivot) {
            exchange(arr, ++i, j);
        }
    }

    exchange(arr, ++i, upper - 1);

    // Return index of pivot
    return i + 1;
}

/**
 * Iterative implementation of insertion sort, with array bounds
 *
 * @param arr   Array to sort
 * @param lower Lower bound to sort
 * @param upper Upper bound to sort
 */
 void insertion_sort(int* arr, int lower, int upper)
{
    for (int i = lower + 1; i < upper; ++i) {

        int to_sort = arr[i];
        int itr = i;

        // Shift array elements until sorted place is found
        while (itr > lower && to_sort < arr[itr - 1]) {
            arr[itr] = arr[itr - 1];
            --itr;
        }


        arr[itr] = to_sort;
    }
}

/**
 * Find the median of medians in a given portion of an array, when the array is divided into groups of n
 *
 * @param arr   The array to find median of medians of
 * @param lower Lower bound of partitioning
 * @param upper Upper bound of partitioning
 * @param n     Size of the sub-partitions
 * @return      The value of the median of medians
 */
 int median_of_medians(Array* array, int lower, int upper, int n)
{
    if (array->size == 1)
        return array->arr[0];

    int i = lower, idx = 0, res;

    // Medians stores the values of the medians along with their index
    Array* medians = malloc(sizeof(Array));
    medians->size = ceiling((double) (upper - lower) / n);
    medians->arr = malloc(medians->size * sizeof(int));

    // Sort the groups of size n
    while (i + n < upper) {
        insertion_sort(array->arr, i, i + n + 1);
        medians->arr[idx++] = array->arr[(i + n) - ceiling((double) n / 2)];
        i += n;
    }

    // Sort final group if size is less than n
    if (i < upper) {
        insertion_sort(array->arr, i, upper);
        medians->arr[idx] = array->arr[(upper - 1) - ceiling(((double) upper - 1 - i) / 2)];
    }

    // If the size of the medians array is less than n, return the median. Otherwise, recursively select median
    if (medians->size <= n) {
        res = medians->arr[(medians->size - 1) / 2];
        free(medians->arr);
        free(medians);
        return res;
    } else {
        res = select_i(medians, 0, medians->size, n, (medians->size - 1) / 2);
        free(medians->arr);
        free(medians);
        return res;
    }
}

// Get the index of an element in an array (for median of medians)
int index_of(Array* array, int val)
{
    for (int i = 0; i < array->size; ++i) {
        if (array->arr[i] == val)
            return i;
    }
    // Error
    return -1;
}

// Recursive select algorithm. Find the ith smallest element in an array
int select_i(Array* arr, int lower, int upper, int n, int i)
{
    if (upper - lower == 0)
        return arr->arr[lower];

    int x = index_of(arr, median_of_medians(arr, lower, upper, n));
    int q = partition(arr->arr, lower, upper, x);
    int k = q - lower + 1;

    if (k == i)
        return arr->arr[q];
    else if (i < k)
        return select_i(arr, lower, q - 1, n, i);
    else
        return select_i(arr, q + 1, upper, n, i - k);
}
