#ifndef HW2_FILE_H
#define HW2_FILE_H

#include "struct.h"

/**
 * Read an input file consisting of a list of integers on a single line separated by spaces and return a pointer
 * to an integer Array
 *
 * @param filename  input filename
 * @return          new Array containing the file's contents as integers
 */
Array* read(char* filename);

#endif //HW2_FILE_H
