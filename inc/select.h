#ifndef HW2_SELECT_H
#define HW2_SELECT_H

#include <stdlib.h>
#include <string.h>

#include "struct.h"

/**
 * Recursive algorithm to select the i-th smallest element in an array. Time complexity is linear when the value of
 * n is greater than or equal to 5. n should be an odd number, since it is used to find the median of median for groups
 * of size n
 *
 * @param arr   The array to select from
 * @param lower Lower bound to select from (used for recursive calls)
 * @param upper Upper bound to select from (used for recursive calls)
 * @param n     Size of subgroups in median of medians
 * @param i     i-th smallest value
 * @return      The value of the i-th smallest element in the array
 */
int select_i(Array* arr, int lower, int upper, int n, int i);

#endif //HW2_SELECT_H
